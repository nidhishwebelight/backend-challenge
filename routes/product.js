var express = require("express")

const stripe = require("stripe")(process.env.SECRET_KEY)

var router = express.Router()

router.get("/list", async (req, res) => {
   try {
      const products = await stripe.products.list()
      const prices = await stripe.prices.list()
      const skus = await stripe.skus.list()
      Promise.all([products, prices, skus])
         .then(async (response) => {
            const [products, prices, skus] = response
            let productsData = products.data
            let pricesData = prices.data
            let skusData = skus.data
            for (const product of productsData) {
               const sku = skusData.find((sku) => {
                  return sku.product === product.id
               })
               if (!sku) {
                  const price = pricesData.find((price) => {
                     return price.product === product.id
                  })
                  const createdSku = await stripe.skus.create({
                     currency: price.currency,
                     attributes: {
                        size: "",
                        gender: "",
                        color: "",
                     },
                     inventory: { type: "finite", quantity: 500 },
                     price: price.unit_amount,
                     product: product.id,
                  })
                  product.price = price.unit_amount
                  product.sku = createdSku.id
               } else {
                  const price = pricesData.find((price) => {
                     return price.product === product.id
                  })
                  product.price = price.unit_amount
                  product.sku = sku.id
               }
            }
            res.status(200).send(productsData)
         })
         .catch((e) => {
            throw e
         })
   } catch (e) {
      res.status(400).send(e)
   }
})

router.post("/payment", async (req, res) => {
   const {
      sku,
      email,
      quantity,
      currency,
      shippingDetails,
      cardDetails,
   } = req.body
   try {
      const order = await stripe.orders.create({
         currency,
         items: [
            {
               type: "sku",
               parent: sku,
               quantity,
            },
         ],
         shipping: shippingDetails,
      })
      const token = await stripe.tokens.create({
         card: cardDetails,
      })
      const paidOrder = await stripe.orders.pay(order.id, {
         source: token.id,
         email,
      })
      res.status(200).send(paidOrder)
   } catch (e) {
      res.status(400).send(e)
   }
})

module.exports = router
